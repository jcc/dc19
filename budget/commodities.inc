commodity USD
   note US dollars
   format USD 1,000.00
   nomarket

commodity EUR
   note Euros
   format EUR 1,000.00
   nomarket

commodity CHF
   note Swiss francs
   format CHF 1,000.00
   nomarket

commodity BRL
   note Real
   format BRL 1.000,00
   nomarket
   default
