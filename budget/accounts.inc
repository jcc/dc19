account assets:cash
account assets:debian-france
account assets:ICTL
account assets:SPI

account expenses:bursaries:bursaries
account expenses:bursaries:diversity
account expenses:bursaries:travel
account expenses:child care
account expenses:content
account expenses:daytrip
account expenses:fees
account expenses:graphic materials:banner hotel
account expenses:graphic materials:banner entrance
account expenses:graphic materials:banner logos
account expenses:graphic materials:banner room
account expenses:graphic materials:banner signs
account expenses:graphic materials:banner generic
account expenses:graphic materials:poster a3
account expenses:incidentals:bank
account expenses:incidentals:frontdesk
account expenses:incidentals:misc
account expenses:incidentals:transport
account expenses:insurance:people
account expenses:insurance:equipaments
account expenses:meetings
account expenses:party:cheese and wine
account expenses:party:conference dinner
account expenses:party:july fest
account expenses:postal
account expenses:press
account expenses:rent:computers
account expenses:rent:sound equipaments
account expenses:roomboard:accommodation:bedrooms
account expenses:roomboard:accommodation:cleaning
account expenses:roomboard:accommodation:vegan food
account expenses:roomboard:bar beer
account expenses:roomboard:food:catering
account expenses:roomboard:food:coffee and tea
account expenses:roomboard:food:food
account expenses:sponsors
account expenses:swag:backpack
account expenses:swag:badge paper
account expenses:swag:drink cup
account expenses:swag:havaianas
account expenses:swag:lanyard
account expenses:swag:scarf
account expenses:swag:t-shirt
account expenses:travel costs for invited speaker
account expenses:venue:staff
account expenses:video
account expenses:internet link
account incomes:registration
account incomes:donation
account incomes:roomboard:accommodation
account incomes:roomboard:bar
account incomes:sponsors:bronze
account incomes:sponsors:gold
account incomes:sponsors:platinum
account incomes:sponsors:silver
account incomes:sponsors:supporter

account liabilities:helen
account liabilities:phls
account liabilities:lenharo
