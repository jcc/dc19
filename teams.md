# People from Brazil in DebConf Teams

## Sponsorship & Fundraising

sponsors@debconf.org

* Helen
* Paulo
* Samuel
* Siqueira

## Visa

visa@debconf.org

* Daniel
* Giovani
* Samuel

## Registration & Front Desk

registration@debconf.org

* Daniel
* Paulo

## Bursaries

bursaries@debconf.org

* Adriana (not yet)

## Content

content@debconf.org

## Catering

* Adriana
* foz
* Giovani
* Paulo

## Accomodation/Hotel

* Daniel
* Paulo
* Terceiro

## Harassment

antiharassment@debian.org

## Venue/UTFPR

* Daniel
* Samuel
* Terceiro

## Video

debconf-video@lists.debian.org

<https://lists.debian.org/debconf-video>

***

See:

<https://wiki.debian.org/DebConf/19/TeamRoles>

